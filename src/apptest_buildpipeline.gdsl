node {

    // Mark the code checkout 'Checkout'....
    stage 'Checkout'

    // // Get some code from a GitHub repository
    git credentialsId: 'github', url: 'https://github.com/jimmycal/terraform-configs.git'

    wrap([$class: 'AnsiColorBuildWrapper']) {

        // Mark the code build 'plan'....
        stage name: 'Plan', concurrency: 1
        dir('app_test_env') {
            // Output Terraform version
            sh "terraform --version"
            //Remove the terraform state file so we always start from a clean state

            if (fileExists("terraform.tfstate")) {
                sh "set +e; rm terraform.tfstate"
            }

            if (fileExists("terraform.tfstate.backup")) {
                sh "set +e; rm terraform.tfstate.backup"
            }

            if (fileExists("plan.out")) {
                sh "set +e; rm plan.out"
            }


            if (fileExists("status")) {
                sh "set +e; rm status"
            }

            if (fileExists("error")) {
                sh "set +e; rm error"
            }

            //Load Remote State If Exists
            sh "set +e; bmcs os object get -ns oraclejamescalise -bn $Identifier-Test  --name terraform.tfstate --file ./terraform.tfstate; echo \$? > error"

            if (fileExists("error")) {
                sh "bmcs os bucket create -ns oraclejamescalise --name $Identifier-Test  --compartment-id ocid1.compartment.oc1..aaaaaaaa3ani73fln4bra7s6jj6a7gmslperpi4fgbfrevjgroyxnaialnpq"
            } else {
                sh "bmcs os object get -ns oraclejamescalise -bn $Identifier-Test --name terraform.tfstate --file ./terraform.tfstate"
                sh "bmcs os object get -ns oraclejamescalise -bn $Identifier-Test --name jenkins.tfvars --file ./jenkins.tfvars"
            }
            //Load Remote State

            //Update Vars file with build specific attributes
            sh "sed -i 's/region = \".*\"/region = \"$Region\"/g' jenkins.tfvars"
            sh "sed -i 's/ad = .*/ad = $AD/g' jenkins.tfvars"
            sh "sed -i 's/compartment_name = \".*\"/compartment_name = \"$Compartment\"/g' jenkins.tfvars"
            sh "sed -i 's/identifier = \".*\"/identifier = \"$Identifier\"/g' jenkins.tfvars"
            sh "sed -i 's/shape_name = \".*\"/shape_name = \"$Shape\"/g' jenkins.tfvars"
            sh "sed -i 's/manage_with_omc = .*/manage_with_omc = $OMC_Manage/g' jenkins.tfvars"
            sh "sed -i 's/customer = .*/customer = \"$Customer\"/g' jenkins.tfvars"
            //Update Vars file with build specific attributes

            sh "terraform get -update"
            sh "set +e; terraform plan -var-file=jenkins.tfvars -out=plan.out -detailed-exitcode; echo \$? > status"


            def exitCode = readFile('status').trim()
            def apply = false
            echo "Terraform Plan Exit Code: ${exitCode}"
            if (exitCode == "0") {
                currentBuild.result = 'SUCCESS'
            }
            if (exitCode == "1") {
                //slackSend channel: '#ci', color: '#0080ff', message: "Plan Failed: ${env.JOB_NAME} - ${env.BUILD_NUMBER} ()"
                currentBuild.result = 'FAILURE'
                sh "bmcs os bucket delete -ns oraclejamescalise --name $Identifier-Test --force"
            }
            if (exitCode == "2") {
                //stash name: "plan", includes: "plan.out"
                //slackSend channel: '#ci', color: 'good', message: "Plan Awaiting Approval: ${env.JOB_NAME} - ${env.BUILD_NUMBER} ()"
                try {
                    //input message: 'Apply Plan?', ok: 'Apply'
                    apply = true
                } catch (err) {
                    //    slackSend channel: '#ci', color: 'warning', message: "Plan Discarded: ${env.JOB_NAME} - ${env.BUILD_NUMBER} ()"
                    apply = false
                    currentBuild.result = 'UNSTABLE'
                    sh "bmcs os bucket delete -ns oraclejamescalise --name $Identifier-Test --force"
                }
            }

            if (apply) {
                stage name: 'Apply', concurrency: 1
                //unstash 'plan'
                if (fileExists("status.apply")) {
                    sh "rm status.apply"
                }
                echo "Apply"
                sh 'set +e; terraform apply plan.out; echo \$? > status.apply'
                def applyExitCode = readFile('status.apply').trim()
                if (applyExitCode == "0") {
                    sh "bmcs os object put -ns oraclejamescalise -bn $Identifier-Test --name terraform.tfstate --file ./terraform.tfstate"
                    sh "bmcs os object put -ns oraclejamescalise -bn $Identifier-Test --name jenkins.tfvars --file ./jenkins.tfvars"

                    //sh "myenv=\$(bmcs os bucket list -ns oraclejamescalise --compartment-id ocid1.compartment.oc1..aaaaaaaa3ani73fln4bra7s6jj6a7gmslperpi4fgbfrevjgroyxnaialnpq | jq -r '.data[] | .name'); touch /var/lib/jenkins/terraform_envs/\$myenv"
                    sh "touch /var/lib/jenkins/terraform_envs/$Identifier-Test"
                    //slackSend channel: '#ci', color: 'good', message: "Changes Applied ${env.JOB_NAME} - ${env.BUILD_NUMBER} ()"
                } else {
                    //slackSend channel: '#ci', color: 'danger', message: "Apply Failed: ${env.JOB_NAME} - ${env.BUILD_NUMBER} ()"
                    currentBuild.result = 'FAILURE'
                    sh "bmcs os bucket delete -ns oraclejamescalise --name $Identifier-Test --force"
                    sh "set +e; terraform destroy -force -var-file=jenkins.tfvars"
                }
            }
        }
    }
}
